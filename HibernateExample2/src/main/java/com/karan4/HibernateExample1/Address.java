package com.karan4.HibernateExample1;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="empAdr")
public class Address {
	
	@Id @GeneratedValue //@PrimaryKeyJoinColumn
	private int adId;
	
//	@OneToOne(targetEntity=Employee.class)		//Thisll make it bidirectional reference of columns 
//	private Employee empRef;
	
	public int getAdId() {
		return adId;
	}
	public void setAdId(int adId) {
		this.adId = adId;
	}
//	public Employee getEmpRef() {
//		return empRef;
//	}
//	public void setEmpRef(Employee empRef) {
//		this.empRef = empRef;
//	}
	
	private String street;
	private String city;
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "Address [adId=" + adId  + ", street=" + street + ", city=" + city + "]";
	}
	
}
