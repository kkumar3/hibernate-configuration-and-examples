package com.karan4.HibernateExample1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class Main {
//This program defines a one t one mapping between employee and address
	

	
	public static void main(String[] args) {
		
				//hibernate xml will define the operation we need to perform
		
				SessionFactory sf = new Configuration().configure().buildSessionFactory();
				Session session =  sf.openSession();
				session.beginTransaction();
				Employee e1 = new Employee();
				//e1.setEmpId(empId); //as this is auto generated we are not setting it.
				e1.setEmpName("Hitesh");
				e1.setEsal(56879);
				
				Address a1 = new Address();
				
				a1.setCity("PItts");
				
				a1.setStreet("Foora");
				
				e1.setAdd(a1);
				
				session.save(e1);
				
				session.getTransaction().commit();
				
				Employee e =(Employee) session.get(Employee.class, 1);
				System.out.println(e);
		
	}
}
