package com.karan4.HibernateExample1;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="emponetoone")
public class Employee {
	
	@Id @GeneratedValue @PrimaryKeyJoinColumn //this annotation defines the join based on primary key
	@Column(name="eno")
	private int empId;
	
	@Column(name="ename")
	private String empName;
	
	@Column(name="esal")
	private int esal;
	
	@OneToOne(targetEntity=Address.class, cascade = CascadeType.ALL ) //cascade type all will link objects here so change in mapped employee will reflect in Address
	private Address add;
	
	public Address getAdd() {
		return add;
	}

	public void setAdd(Address add) {
		this.add = add;
	}
	
	
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", esal=" + esal + ", add=" + add + "]";
	}
	
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public int getEsal() {
		return esal;
	}
	public void setEsal(int esal) {
		this.esal = esal;
	}
	
	
}
