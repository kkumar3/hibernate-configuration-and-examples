package com.karan4.HibernateExample1;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="owner_empbatch38")

@AttributeOverrides({
	@AttributeOverride(name="firstname", column=@Column(name="FIRSTNAME")),
	@AttributeOverride(name="lastname", column=@Column(name="LASTNAME"))
})
public class Owner extends Person {
	

	
	public Owner(String firstname, String lastname, Long stocks, Long partnershipStake) {
		super(firstname, lastname);
		this.partnershipStake = partnershipStake;
		this.stocks = stocks;
	}

	@Column(name="stocks")
	private Long stocks;
	
	@Override
	public String toString() {
		return "Owner [stocks=" + stocks + ", partnershipStake=" + partnershipStake + "]";
	}

	public Long getStocks() {
		return stocks;
	}

	public void setStocks(Long stocks) {
		this.stocks = stocks;
	}

	public Long getPartnershipStake() {
		return partnershipStake;
	}

	public void setPartnershipStake(Long partnershipStake) {
		this.partnershipStake = partnershipStake;
	}

	@Column(name="partnership_stake")
	private Long partnershipStake;
	
	
}
