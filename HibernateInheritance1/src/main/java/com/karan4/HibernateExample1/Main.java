package com.karan4.HibernateExample1;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;




public class Main {

	

	
	public static void main(String[] args) {
		
				
				Configuration cf= new Configuration();
		
				SessionFactory sf = cf.configure().buildSessionFactory();
				Session session =  sf.openSession();
				session.beginTransaction();
				
				Person p1 = new Person("Steve", "Waugh");
				p1.setPersonId(1L);
				session.save(p1);
				
				Developer employee = new Developer("James","Gosling", "Sales", new Date());
				employee.setPersonId(2L);
				
				session.save(employee);
				
				Owner owner = new Owner("Bill", "Gates", 300L, 20L);
				owner.setPersonId(3L);
				session.save(owner);

			
				session.getTransaction().commit();
				session.close();
				
		
	}
}
