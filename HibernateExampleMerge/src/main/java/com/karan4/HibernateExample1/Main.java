package com.karan4.HibernateExample1;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class Main {
//	static Employee emp;
//	static Session session;
//	static SessionFactory sfactory;
	

	
	public static void main(String[] args) {
		
				//session 1
				
				Employee e1 = null;
				
				try(Session session = HibernateUtil.getSessionFactory().openSession()){
					
					e1 = session.get(Employee.class, 1);
					System.out.println(e1);
					
					
					
				} catch (HibernateException e) {e.printStackTrace();}
				
			
				e1.setEsal(50000);
				
				//session 2
				
				try(Session session =HibernateUtil.getSessionFactory().openSession()){
					session.beginTransaction();
					
					session.merge(e1);
					
					session.getTransaction().commit();
					
					System.out.println(e1);
				} catch (HibernateException e) {e.printStackTrace();}
				}
		
	}

