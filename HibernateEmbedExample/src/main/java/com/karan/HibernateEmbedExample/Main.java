package com.karan.HibernateEmbedExample;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class Main 
{
    public static void main( String[] args )
    {
        Student s1 = new Student();
        
        s1.setsName("Karan");
        
        Address adr = new Address();
        adr.setCity("New York");
        adr.setStreet("Belevue St");
        
        s1.setAddress(adr);
        
        SessionFactory sf = new Configuration().configure().buildSessionFactory();
        
        	Session session1 = sf.openSession();
        	session1.beginTransaction();
        	session1.save(s1);
        	
        	session1.getTransaction().commit();
        	
        	System.out.println("my input object with embed");
        	
        	session1.close();
    }
}
